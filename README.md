# Welcome to RebajaTusCuentas.com

Please complete this guide by uploading your work to your own gitlab repository 
and doing a MR to this one. The branch must contain the readme file with the
responses using markdown and referencing to folders or files
that you need in order to solve the test.


## 1

We encourage documentation and investigation in order to have agile development.
We support RTFM and LMGTFY:

>___Create a file telling us when you last used RTFM and LMGTFY,
the OS you use and the languages you master___

# RTFM
At the beginning of this year, the Ministry of Energy and Mines assigned me a service order to migrate an on-premise SQL Server database, related to the institution's patrimonial assets, to Oracle's cloud infrastructure. Although I had basic knowledge of some of these cloud tools, thanks to an introductory course I had taken, it was necessary to thoroughly study the entire process of migrating on-premise databases to Oracle Database. For this, I consulted the [migration manual to Oracle Database](https://www.oracle.com/technical-resources/articles/cloud/migrate-db-to-cloud-with-datapump.html) and studied the documentation of specific tools such as [Oracle Data Pump](https://docs.oracle.com/es-ww/iaas/autonomous-database/doc/use-oracle-data-pump.html#:~:text=Oracle%20Data%20Pump%20offers%20a,dedicated%20Exadata%20infrastructure.) and [Oracle Cloud Infrastructure Database Migration Service](https://docs.oracle.com/en-us/iaas/database-migration/doc/overview-oracle-cloud-infrastructure-database-migration.html).

# LMGTFY 
To develop static websites, I used to rely solely on the React library. However, after a conversation with a friend from university who is also a developer, he told me about the Astro framework. So, I immediately started researching Astro and discovered that it is renowned for its efficiency in creating static sites. Through searches on Google and reviewing tutorials on YouTube, I learned how to use this framework. As a result, the static websites I have created, like my personal website, now load significantly faster. This not only improves user experience but also contributes to the long-term positioning of my website.

# The OS you use 
I primarily use Windows as my operating system due to my familiarity with it. However, I also integrate Windows Subsystem for Linux (WSL) into my workflow to access Linux-based environments. This allows me to execute commands that require privileges, as within WSL I can invoke the sudo command without any issue.

# The languages you master
I am proficient in **JavaScript**, commonly used for web development, **Python**, favored for machine learning scripting tasks, and **C#**,  utilized in game development with Unity.

## 2

Automation helps us to avoid human errors. Some of our systems use CI.

>___Write a program in the language of your choice that can accomplish this.
You can use Pseudocode.___

### Pseudocode

1. **Get the Latest Uploaded Code:**
   - Fetch the most recent version of the codebase from the version control system.

2. **Run Code Review Tool (Using ESLint for Example):**
   - If errors are found:
     - Notify the developer of the errors.
     - Stop the process until the errors are fixed.
   - Else:
     - Proceed to the next step.

3. **Run Automated Tests (Using Jest and GitHub Actions for Example):**
   - If tests fail:
     - Notify the developer of the failed tests.
     - Stop the process until the errors are fixed.
   - Else:
     - Proceed to the next step.

4. **Deploy to Pre-production Environment and Perform Tests:**
   - Deploy the code to a pre-production environment.
   - Perform tests in the pre-production environment.
   - If pre-production tests fail:
     - Notify the developer.
     - Stop the process and do not deploy to production.
   - Else:
     - Proceed to deploy the code to production.


>___If you are not familiar with writing these programs, you can explain the
most representative concepts.___

Take a look at my flowchart.

<p align="center">
  <img src="./image/flowchart.webp" alt="Flowchart">
</p>

## 3

A developer's portfolio is important to us. We ask you to upload 1 or 2 
projects of your authorship.

>___If you do not want to share the code, you can just paste some of it.___

# My Personal Projects

## Personal Website

**Description**: A personal webpage showcasing my portfolio, blog, and more about me. Built with modern technologies, it represents my digital footprint on the web.

**Technologies Used**: JavaScript, Astro

- **GitHub Code**: [My Github](https://github.com/jorge-holguin/Personal-Website.git)
- **View Website**: [My Website](https://jorgealbertoh1.sg-host.com/)

## El Escape de Mota - Infinity Runner 2D

**Description**: A 2D infinite runner video game developed in Unity, inspired by the iconic Google Chrome dinosaur game, but starring "Mota," my Schnauzer dog. A fun twist on a beloved classic, tailored with personal affection.

**Technologies Used**: Unity, C#

- **GitHub Code**: [El Escape de Mota](https://github.com/jorge-holguin/El-Escape-de-Mota)
- **Try the Game**: [Play on itch.io](https://jorge-holguin.itch.io/el-escape-de-mota)

## Machine Learning Predictive Model for Roulette

**Description**: A predictive model using the Machine Learning algorithm Random Forest for betting in roulette. Designed to analyze and predict winning numbers with higher accuracy, enhancing the traditional roulette experience with a touch of AI.

**Technologies Used**: Unity, C#

- **GitHub Code**: [Code](https://github.com/jorge-holguin/Ruleta_Mejorada)
- **Try the Script**: [Colab Notebook](https://colab.research.google.com/drive/14nFE1oesKsuZU-Dj93zXzrph1wtF9PmM?usp=sharing)

## 4

>___Please, write a code or pseudocode that solves the problem in which I have a 
collection of numbers in ascending order. You need to find the matching pair 
that it is equal to a sum that its also given to you. If you make any 
assumption, let us know.___

>___Example:___
* [2,3,6,7]  sum = 9  - OK, matching pair (3,6)
* [1,3,3,7]  sum = 9  - No

* Consider recibe 1 000 000 numbers

I've assumed the following:

* I'm assuming the example omitted to include the pair (2,7). I'm assuming it was a mistake since the problem statement says that pairs equal to the sum should be found.

* Pairs Can Share Numbers: A number from the collection can be used in more than one pair if it fits the sum requirement with different numbers.

* Ascending Order: The user is expected to input the collection in ascending order, and the program checks for this.

* Only Integers Values: Only integer values are considered for both the numbers in the collection and the target sum.

* Single Execution for Each Input: The program processes one set of inputs per execution. Changes to the collection or sum require a new execution.

### Note: 

To efficiently handle an array of 1,000,000 numbers without using nested loops, we can use the two-pointer technique, which is suitable for sorted arrays. This method eliminates the need to traverse the array multiple times, crucial for managing large volumes of data efficiently.

* For this exercise, I'll provide the solution both in pseudocode and through a JavaScript script. You can find the script in the 'src' folder and the test in the 'test' folder.

### Pseudocode:
```
Procedure FindMatchingPairs
  Input: numbersArray, targetSum
  Assume numbersArray is already sorted in ascending order
  Initialize an empty array to hold the pairs
  pairsList <- []
  // Initialize two pointers
  left <- 0
  right <- length of numbersArray - 1
  While left < right
    If numbersArray[left] + numbersArray[right] == targetSum Then
      // If the sum of the values at the two pointers equals targetSum, add the pair to pairsList
      Add (numbersArray[left], numbersArray[right]) to pairsList
      // Move both pointers towards the center
      left <- left + 1
      right <- right - 1
    Else If numbersArray[left] + numbersArray[right] < targetSum Then
      // If the sum is less than targetSum, move the left pointer to the right to increase the sum
      left <- left + 1
    Else
      // If the sum is more than targetSum, move the right pointer to the left to decrease the sum
      right <- right - 1
    End If
  End While
  // Check if any pairs were found
  If pairsList is not empty Then
    Display "Matching pairs found:" and pairsList
  Else
    Display "No matching pairs found."
  End If
End Procedure
```
## 5

"The message is in spanish."

>___4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c20616772
6567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c
736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374
612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f7220
617175ed212e___


>___U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVu
IG1lbnNhamUu___

"My output." (in Spanish)

>___486f6c612c204d616e75656c2e2054652073616c756461204a6f72676520486f6c6775c3ad6e2e20486520726563696269646f207475206d656e73616a652e204c616d656e7461626c656d656e74652c2048616e676f757473207961206e6f2065786973746520646573646520616e6f2070617361646f20f09f98a22e2053696e20656e746572626172676f2c20746520686520616772656761646f2061206d697320636f6e746163746f7320646520476f6f676c6520436861742e___

>___QWwgcGFyZWVyIGhlIGxvZ3JhZG8gbGVnYXIgdXR0YSBlc3RhIHBhcnRlLiBFc3Blcm8gY29uIGFuc2lhcyB0dSBycXVlc3RwYSDilw==___


# All answers must be inside a docker image and the answer will be tested with a running container. Add lint and at least 01 unit test
