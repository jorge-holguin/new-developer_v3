>___Create a file telling us when you last used RTFM and LMGTFY,
the OS you use and the languages you master___

# RTFM
At the beginning of this year, the Ministry of Energy and Mines assigned me a service order to migrate an on-premise SQL Server database, related to the institution's patrimonial assets, to Oracle's cloud infrastructure. Although I had basic knowledge of some of these cloud tools, thanks to an introductory course I had taken, it was necessary to thoroughly study the entire process of migrating on-premise databases to Oracle Database. For this, I consulted the [migration manual to Oracle Database](https://www.oracle.com/technical-resources/articles/cloud/migrate-db-to-cloud-with-datapump.html) and studied the documentation of specific tools such as [Oracle Data Pump](https://docs.oracle.com/es-ww/iaas/autonomous-database/doc/use-oracle-data-pump.html#:~:text=Oracle%20Data%20Pump%20offers%20a,dedicated%20Exadata%20infrastructure.) and [Oracle Cloud Infrastructure Database Migration Service](https://docs.oracle.com/en-us/iaas/database-migration/doc/overview-oracle-cloud-infrastructure-database-migration.html).

# LMGTFY 
To develop static websites, I used to rely solely on the React library. However, after a conversation with a friend from university who is also a developer, he told me about the Astro framework. So, I immediately started researching Astro and discovered that it is renowned for its efficiency in creating static sites. Through searches on Google and reviewing tutorials on YouTube, I learned how to use this framework. As a result, the static websites I have created, like my personal website, now load significantly faster. This not only improves user experience but also contributes to the long-term positioning of my website.

# The OS you use 
I primarily use Windows as my operating system due to my familiarity with it. However, I also integrate Windows Subsystem for Linux (WSL) into my workflow to access Linux-based environments. This allows me to execute commands that require privileges, as within WSL I can invoke the sudo command without any issue.

# The languages you master
I am proficient in **JavaScript**, commonly used for web development, **Python**, favored for machine learning scripting tasks, and **C#**,  utilized in game development with Unity.