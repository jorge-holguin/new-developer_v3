FROM node:latest

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run lint

ENV CI=true

CMD ["npm", "test"]
