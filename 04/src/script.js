/**
 * Finds pairs of numbers in a string that sum up to a target.
 * @param {string} numbersString - String of numbers separated by commas.
 * @param {number} sumTarget - Target sum for finding pairs.
 * @return {string} Message indicating matching pairs or no matches.
 */
function findPairs(numbersString, sumTarget) {
  // Convert the input string to an array of integers
  const numbersArray = numbersString.split(',')
      .map((item) => parseInt(item, 10));

  // Check if the array is in ascending order
  const isAscending = numbersArray.every((value, index, array) =>
    index === 0 || (value >= array[index - 1]));

  if (!isAscending) {
    // Return a message if numbers are not in ascending order
    return 'Please enter the numbers in ascending order';
  }

  // Initialize pointers for the two-pointer technique
  let left = 0;
  let right = numbersArray.length - 1;
  const pairs = [];

  // Iterate while left pointer is less than right pointer
  while (left < right) {
    // Calculate current sum of the values at the left and right pointers
    const currentSum = numbersArray[left] + numbersArray[right];

    // Check if current sum matches the target sum
    if (currentSum === sumTarget) {
      // Add the current pair to the pairs array
      pairs.push(`(${numbersArray[left]}, ${numbersArray[right]})`);
      left++; // Move left pointer right
      right--; // Move right pointer left
    } else if (currentSum < sumTarget) {
      left++; // Move left pointer right if current sum is less than target
    } else {
      right--; // Move right pointer left if current sum is more than target
    }
  }

  // Return the found pairs or a message indicating none were found
  return pairs.length > 0 ?
  `Matching pairs: ${pairs.join(', ')}` : 'No matching pairs found.';
}

/**
 * Setup event listener for the "findPairsBtn" button. This function should be
 * called in an environment where the DOM is available, such as in a browser.
 */
function setupEventListener() {
  // Check if the DOM is available
  if (typeof document !== 'undefined') {
    const btn = document.getElementById('findPairsBtn');
    if (btn) { // Check if the element exists before adding the listener
      btn.addEventListener('click', function() {
        const numbersString = document.getElementById('numbersInput').value;
        const sumTarget = parseInt(document
            .getElementById('sumInput').value, 10);
        const result = findPairs(numbersString, sumTarget);
        document.getElementById('result').innerHTML = result;
      });
    }
  }
}

// Execute the setup function to add event listener
setupEventListener();

// Export the findPairs function and setupEventListener
export {findPairs, setupEventListener};
