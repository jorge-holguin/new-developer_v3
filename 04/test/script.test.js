import {findPairs} from '../src/script';

describe('findPairs function', () => {
  test('finds pairs that sum up to the target sum', () => {
    const numbersString = '1,2,3,4';
    const sum = 5;
    expect(findPairs(numbersString, sum))
        .toBe('Matching pairs: (1, 4), (2, 3)');
  });

  test('returns a message if no pairs are found', () => {
    const numbersString = '1,2,3';
    const sum = 7;
    expect(findPairs(numbersString, sum)).toBe('No matching pairs found.');
  });

  test('returns a message if the numbers are not in ascending order', () => {
    const numbersString = '4,3,2,1';
    const sum = 3;
    expect(findPairs(numbersString, sum))
        .toBe('Please enter the numbers in ascending order');
  });
});
