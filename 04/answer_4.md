## 4

>___Please, write a code or pseudocode that solves the problem in which I have a 
collection of numbers in ascending order. You need to find the matching pair 
that it is equal to a sum that its also given to you. If you make any 
assumption, let us know.___

>___Example:___
* [2,3,6,7]  sum = 9  - OK, matching pair (3,6)
* [1,3,3,7]  sum = 9  - No

* Consider recibe 1 000 000 numbers


I've assumed the following:

* I'm assuming the example omitted to include the pair (2,7). I'm assuming it was a mistake since the problem statement says that pairs equal to the sum should be found.

* Pairs Can Share Numbers: A number from the collection can be used in more than one pair if it fits the sum requirement with different numbers.

* Ascending Order: The user is expected to input the collection in ascending order, and the program checks for this.

* Only Integers Values: Only integer values are considered for both the numbers in the collection and the target sum.

* Single Execution for Each Input: The program processes one set of inputs per execution. Changes to the collection or sum require a new execution.

### Note: 

To efficiently handle an array of 1,000,000 numbers without using nested loops, we can use the two-pointer technique, which is suitable for sorted arrays. This method eliminates the need to traverse the array multiple times, crucial for managing large volumes of data efficiently.

* For this exercise, I'll provide the solution both in pseudocode and through a JavaScript script. You can find the script in the 'src' folder and the test in the 'test' folder.

### Pseudocode:
```
Procedure FindMatchingPairs
  Input: numbersArray, targetSum
  Assume numbersArray is already sorted in ascending order
  Initialize an empty array to hold the pairs
  pairsList <- []
  // Initialize two pointers
  left <- 0
  right <- length of numbersArray - 1
  While left < right
    If numbersArray[left] + numbersArray[right] == targetSum Then
      // If the sum of the values at the two pointers equals targetSum, add the pair to pairsList
      Add (numbersArray[left], numbersArray[right]) to pairsList
      // Move both pointers towards the center
      left <- left + 1
      right <- right - 1
    Else If numbersArray[left] + numbersArray[right] < targetSum Then
      // If the sum is less than targetSum, move the left pointer to the right to increase the sum
      left <- left + 1
    Else
      // If the sum is more than targetSum, move the right pointer to the left to decrease the sum
      right <- right - 1
    End If
  End While
  // Check if any pairs were found
  If pairsList is not empty Then
    Display "Matching pairs found:" and pairsList
  Else
    Display "No matching pairs found."
  End If
End Procedure
```


