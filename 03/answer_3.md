A developer's portfolio is important to us. We ask you to upload 1 or 2 
projects of your authorship.

>___If you do not want to share the code, you can just paste some of it.___

# My Personal Projects// Add an event listener to handle the click event on the button "findPairsBtn"

## Personal Website

**Description**: A personal webpage showcasing my portfolio, blog, and more about me. Built with modern technologies, it represents my digital footprint on the web.

**Technologies Used**: JavaScript, Astro

- **GitHub Code**: [My Github](https://github.com/jorge-holguin/Personal-Website.git)
- **View Website**: [My Website](https://jorgealbertoh1.sg-host.com/)

## El Escape de Mota - Infinity Runner 2D

**Description**: A 2D infinite runner video game developed in Unity, inspired by the iconic Google Chrome dinosaur game, but starring "Mota," my Schnauzer dog. A fun twist on a beloved classic, tailored with personal affection.

**Technologies Used**: Unity, C#

- **GitHub Code**: [El Escape de Mota](https://github.com/jorge-holguin/El-Escape-de-Mota)
- **Try the Game**: [Play on itch.io](https://jorge-holguin.itch.io/el-escape-de-mota)

## Machine Learning Predictive Model for Roulette

**Description**: A predictive model using the Machine Learning algorithm Random Forest for betting in roulette. Designed to analyze and predict winning numbers with higher accuracy, enhancing the traditional roulette experience with a touch of AI.

**Technologies Used**: Unity, C#

- **GitHub Code**: [Code](https://github.com/jorge-holguin/Ruleta_Mejorada)
- **Try the Script**: [Colab Notebook](https://colab.research.google.com/drive/14nFE1oesKsuZU-Dj93zXzrph1wtF9PmM?usp=sharing)

