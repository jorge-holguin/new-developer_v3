## 2
Automation helps us to avoid human errors. Some of our systems use CI.

>___Write a program in the language of your choice that can accomplish this.
You can use Pseudocode.___

For this exercise, I'll provide the solution both in pseudocode. Additionally, I aim to replicate the CI process using a very simple script created with JavaScript. You can find the script in the 'src' folder and the test in the 'test' folder.

### Pseudocode

1. **Get the Latest Uploaded Code:**
   - Fetch the most recent version of the codebase from the version control system.

2. **Run Code Review Tool (Using ESLint for Example):**
   - If errors are found:
     - Notify the developer of the errors.
     - Stop the process until the errors are fixed.
   - Else:
     - Proceed to the next step.

3. **Run Automated Tests (Using Jest and GitHub Actions for Example):**
   - If tests fail:
     - Notify the developer of the failed tests.
     - Stop the process until the errors are fixed.
   - Else:
     - Proceed to the next step.

4. **Deploy to Pre-production Environment and Perform Tests:**
   - Deploy the code to a pre-production environment.
   - Perform tests in the pre-production environment.
   - If pre-production tests fail:
     - Notify the developer.
     - Stop the process and do not deploy to production.
   - Else:
     - Proceed to deploy the code to production.

>___If you are not familiar with writing these programs, you can explain the
most representative concepts.___

Take a look at my flowchart.

<p align="center">
  <img src="../image/flowchart.webp" alt="Flowchart">
</p>

