/**
  Sorts an array of numbers in ascending order.
  @param {number[]} numbers - The array of numbers to sort.
  @return {number[]} - A new array with the numbers sorted in ascending order.
 */
function sortNumbers(numbers) {
  return numbers.sort((a, b) => a - b);
}
// Export the sortNumbers function
module.exports = sortNumbers;
