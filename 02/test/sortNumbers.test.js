// Import the sortNumbers function you want to test
const sortNumbers = require('../src/sortNumbers.js');

// Define a test suite for the sortNumbers function
describe('sortNumbers function', () => {
  // Define a test case to verify that the function sorts numbers correctly
  test('sorts an array of numbers in ascending order', () => {
    // Define an array of unsorted numbers
    const numbers = [3, 1, 4, 2, 5];
    // Call the sortNumbers function with the array of unsorted numbers
    const sortedNumbers = sortNumbers(numbers);
    // Verify result is new array with sorted numbers.
    expect(sortedNumbers).toEqual([1, 2, 3, 4, 5]);
  });
});
